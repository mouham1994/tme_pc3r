mtype {ROUGE, ORANGE,VERT, IND}
chan obs = [0] of {mtype, bool}

active proctype feu(){
    bool clignotant = false
    mtype couleur = IND

    initial : atomic {
        couleur = ORANGE
        clignotant = true
        obs!couleur,clignotant
    }
    if
        :: true -> clignotant = false; goto goRed
        :: true -> goto initial 
    fi
    
    goRed : atomic {
        couleur = ROUGE
        obs! couleur , clignotant
    }
    if  :: true -> goto goGreen
        :: true -> goto goRed
        :: true -> goto fault
    fi

    goGreen:atomic {
        couleur = VERT
        obs! couleur , clignotant
    }
    if  :: true -> goto goGreen
        :: true -> goto goOrange
        :: true -> goto fault
    fi

    goOrange:atomic {
       	couleur = ORANGE
        obs! couleur , clignotant
    }
    if  :: true -> goto goOrange
        :: true -> goto goRed
        :: true -> goto fault
    fi


    fault : atomic {
        couleur = ORANGE
        clignotant = true
        obs ! couleur , clignotant
    }
    if :: true -> goto fault
    fi

}   

active proctype observateur (){
    mtype prec = IND;
    mtype couleur = IND;
    bool clignotant = false;

    do:: obs?couleur,clignotant ->
        if:: atomic {
            couleur == ORANGE -> assert(clignotant == true || prec != ROUGE )
            prec = ORANGE
            }
          ::atomic {
              couleur == VERT -> assert(prec != ORANGE)
              prec = VERT 
          }
          ::atomic {
              couleur == ROUGE -> assert (prec != VERT)
              prec = ROUGE
          }
          ::atomic{
			clignotant == true -> assert(couleur == ORANGE) 
			
          }
        fi
	od
}

