chan prod = [0] of {int}
chan gest = [0] of {int}
chan ouv = [0] of {int}
chan col = [0] of {bool}

active proctype producteur(){
	i =  0;
	do
	:: i < 20 ->
		prod!3 //generer un nombre aleatoir entre 1 et 5
	
	:: i == 20 -> break
	od
} 

active proctype gestionnaire(){
	do
		::prod ? x -> cOuv! x
		::gest ? y -> cOuv! y
	od
} 

active proctype  ouvrier(){
	
	do
		::ouv ? p -> 
				p = p-1;
				if
					::p == O -> col! true
					:: else -> gest! p 
				fi
	od

} 

active proctype collecteur(){
	cpt = 0
	do
		::col? true -> 
			cpt = cpt + 1
		
	od	
} 
