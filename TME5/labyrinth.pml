mtype {NORTH, SOUTH, EAST, WEST,ENTER, EXIT}

chan go = [0] of {mtype};
active proctype laby(){
	go!ENTER;
	goto cell_5_5;
	
	cell_1_1:
		if 	
			::true -> go!SOUTH; goto cell_1_2
			::true -> go!EXIT; goto exit
			::true -> go!EAST; goto cell_2_1

		fi
	
	cell_1_2:
		if 	
			::true -> go!NORTH; goto cell_1_1
		fi
	cell_2_1:
		if 	
			::true -> go!WEST; goto cell_1_1
			::true -> go!EAST; goto cell_3_1
		fi

	cell_3_1:
		if 	
			::true -> go!WEST; goto cell_2_1
			::true -> go!EAST; goto cell_4_1
		fi
	


	cell_4_1:
		if 	
			::true -> go!WEST; goto cell_3_1
			::true -> go!SOUTH; goto cell_4_2
		fi
	
	cell_4_2:
		if 	
			::true -> go!NORTH; goto cell_4_1
			::true -> go!WEST; goto cell_3_2
		fi
	
	cell_3_2:
		if 	
			::true -> go!WEST; goto cell_2_2
			::true -> go!EAST; goto cell_4_2
		fi
	cell_2_2:
		if 	
			::true -> go!SOUTH; goto cell_2_3
			::true -> go!EAST; goto cell_3_2
		fi
	cell_2_3:
		if 	
			::true -> go!NORTH; goto cell_2_2
			::true -> go!WEST; goto cell_1_3
			::true -> go!SOUTH; goto cell_2_4
		fi

	cell_1_3:
		if 	
			::true -> go!EAST; goto cell_2_3
		fi

	cell_2_4:
		if 	
			::true -> go!NORTH; goto cell_2_3
			::true -> go!EAST; goto cell_3_4
			::true -> go!SOUTH; goto cell_2_5
		fi

	cell_2_5:
		if 	
			::true -> go!NORTH; goto cell_2_4
			::true -> go!EAST; goto cell_3_5
		fi
	
	cell_3_5:
		if 	
			::true -> go!WEST; goto cell_2_5
			::true -> go!EAST; goto cell_4_5
		fi
	cell_4_5:
		if 	
			::true -> go!WEST; goto cell_3_5
		
		fi

	
	cell_3_4:
		if 	
			::true -> go!WEST; goto cell_2_4
			::true -> go!EAST; goto cell_4_4
		fi
	cell_4_4:
		if 	
			::true -> go!WEST; goto cell_3_4
			::true -> go!EAST; goto cell_5_4
		fi

	cell_5_1:
		if 	
			::true -> go!SOUTH; goto cell_5_2
		fi

	cell_5_2:
		if 	
			::true -> go!NORTH; goto cell_5_1
			::true -> go!SOUTH; goto cell_5_3
		fi

	cell_5_3:
		if 	
			::true -> go!NORTH; goto cell_5_2
			::true -> go!SOUTH; goto cell_5_4
		fi

	cell_5_4:
		if 	
			::true -> go!NORTH; goto cell_5_3
			::true -> go!SOUTH; goto cell_5_5
			::true -> go!WEST; goto cell_4_4
		fi

	cell_5_5:
		if 	
			::true -> go!NORTH; goto cell_5_4
	
		fi


	exit:	

}

active proctype obs (){
	mtype dir;
	do
		:: go? (dir) -> if
						:: dir == EXIT -> printf ("go exit") ; goto exit
						:: else -> printf("go %e\n", dir)
						fi
	od
	exit:
}			
