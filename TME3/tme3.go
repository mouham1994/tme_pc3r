package main

import (
	"fmt" 
  "os"
  "bufio"
  "log"
  "strings"
  "time"
  "strconv"
)

//Structure représentant un paquet
type paquet struct {
  arrivee string
  depart string
  arret int
}


//canalServeur est une structure qui représente l'encapsulation d'un paquet et d'un canal de paquets
type canalServeur struct {
  canalTrav chan paquet
  pqt paquet
}


/* Le lecteur lit le fichier ligne par ligne.
 * et l'envoi aux travailleurs
 */
func lecteur(canalLect chan string) {
  fmt.Println("Le lecteur commence la lecture");
  file, err := os.Open("stop_times.txt");
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()
  scanner := bufio.NewScanner(file)
  scanner.Scan()
  scanner.Text()
  for scanner.Scan() {
    canalLect <- scanner.Text()
  }
  if err := scanner.Err(); err != nil {
    log.Fatal(err)
  }
  fmt.Println("Le lecteur fini la lecture");
}


/* Le travailleur récupère une chaine de caractère depuis le canal de lecture
 * et la transforme en paquet, ensuite il envoit ce paquet au serveur et attend
 * la réponse pour enfin l'envoyer au réducteur
 */
func travailleur(canalLect chan string, canalServ chan canalServeur, canalReduc chan paquet) { 
  for true {
    ligne := <- canalLect
    infos := strings.Split(ligne, ",")
    myCanal := make(chan paquet)
    pqt := paquet{infos[1], infos[2], 0}
    fmt.Println("Paquet envoyé au serveur", pqt)
    canalServ <- canalServeur{myCanal, pqt}
    pqtResult := <- myCanal 
    fmt.Println("Paquet renvoyé par le serveur", pqtResult)
    canalReduc <- pqtResult
  }
}


/* Le réducteur récupère le paquet envoyer par le travailleur, pour ensuite
 * cumuler le temps d'arrêt, et renvoyer à la fin la moyenne lorsqu'il reçoit
 * le signal du processus principal.
 */
func Reducteur(canalReduc chan paquet, canalResult chan int) {
	
	som := 0
	nbr := 0
	
	for true {
    select {
      case <- canalResult:
        canalResult <- som/nbr
        return
      case pqt := <- canalReduc:
        som += pqt.arret
        nbr += 1
        fmt.Println(som)
    }
	}
	
}


/* Conversion d'un string en temps en secondes
 */
func stringToTime(str string) int {
  time := strings.Split(str, ":")
  h, herr := strconv.Atoi(time[0])
  m, merr := strconv.Atoi(time[1])
  s, serr := strconv.Atoi(time[2])
  if (herr != nil) {
    log.Fatal(herr)
  }
  if (merr != nil) {
    log.Fatal(merr)
  }
  if (serr != nil) {
    log.Fatal(serr)
  }
  return h*3600 + m*60 + s
}


/* Reçoit les requettes des diférents travailleurs
 * et pour chaque paquet reçu il délivre la tache pour une sous routine
 * qui une fois à finie son traitement renvoie le résultat au travailleur
 * via son canal privé
 */
func Serveur(canalServ chan canalServeur) {
  for true {
    pqt := <- canalServ
    go func (p canalServeur) {
      pqt := p.pqt
      canalTrav := p.canalTrav
      pqt.arret = stringToTime(pqt.depart) - stringToTime(pqt.arrivee)
      canalTrav <- pqt
    } (pqt)
  }
}


func main() {
  
  canalLect := make(chan string) // Lecteur ==> Travailleurs
  canalServ := make(chan canalServeur) // Travailleur ==> Serveur
  canalResult := make(chan int) // Reducteur ==> Main process
  canalReduc := make(chan paquet) // Travailleur ==> Reducteur
  
  //Lancer les différentes coroutines
  go lecteur(canalLect)
  go travailleur(canalLect, canalServ, canalReduc)
  go travailleur(canalLect, canalServ, canalReduc)
  go travailleur(canalLect, canalServ, canalReduc)
  go travailleur(canalLect, canalServ, canalReduc)
  go Serveur(canalServ)
  go Reducteur(canalReduc, canalResult)
  
  arg := os.Args[1]
  
  t, err := strconv.Atoi(arg)
  if (err != nil) {
    log.Fatal(err)
  }
  
  time.Sleep(time.Duration(t) * time.Second) //Dormir t secondes
  canalResult <- 0 // Envoyer un signal au réducteur pour qu'il s'arrête
  res := <- canalResult // Recevoir le résultat du réducteur
  fmt.Println("Le serveur reçoit du réducteur :", res)

}
