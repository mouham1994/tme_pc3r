use std::sync::{Arc, Mutex};
use std::thread;

// J'ai utiliser une fille de taille 1, ou les producteurs incrementent
// data[0], et les consommateurs decrimente data[0]

fn main() {
    // on crée notre mutex
    let data = Arc::new(Mutex::new(vec![0]));

	// Lancer les producteurs
    for i in 0..3 {
        // on incrémente le compteur interne de Arc
        let data = data.clone();
        thread::spawn(move || {
            let mut ret = data.lock(); // on lock

            // on vérifie qu'il n'y a pas de problème
            match ret {
                Ok(ref mut d) => {
                    // tout est bon, on peut modifier la donnée en toute sécurité !
                    d[i] += 1;
                },
                Err(e) => {
                    // une erreur s'est produite
                    println!("Impossible d'accéder aux données {:?}", e);
                }
            }
        });
    }
    
    // Lancer les consommateur
    for i in 0..3 {
        // on incrémente le compteur interne de Arc
        let data = data.clone();
        thread::spawn(move || {
            let mut ret = data.lock(); // on lock

            // on vérifie qu'il n'y a pas de problème
            match ret {
                Ok(ref mut d) => {
                    // tout est bon, on peut modifier la donnée en toute sécurité !
                    d[i] -= 1;
                },
                Err(e) => {
                    // une erreur s'est produite
                    println!("Impossible d'accéder aux données {:?}", e);
                }
            }
        });
    }

    // on attend 50 millisecondes, le temps que les threads finissent leur travail
    thread::sleep_ms(10000);
}
