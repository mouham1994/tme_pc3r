import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Tapis {

	private int size;
	private int taille;
	private Paquet[] buffer;
	private int tete;
	private int queue;

	private final ReentrantLock lock = new ReentrantLock();
	private final Condition isEmpty = lock.newCondition();
	private final Condition isFull = lock.newCondition();

	public Tapis(int size) {
		this.size = size;
		this.taille = 0;
		this.buffer = new Paquet[size];
		this.tete = 0;
		this.queue = 0;
	}

	public boolean is_vide() {
		return this.taille == 0;
	}

	public boolean is_full() {
		return this.taille == this.size;
	}

	public void enfiler(Paquet p) {
		lock.lock(); // Verrouiller
		try {
			while (this.is_full()) { // Tant que la fille est pleine => dormir sur isFull
				try {
					isFull.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			this.buffer[this.queue] = p;
			this.queue = (this.queue + 1) % this.size;
			this.taille++;
			isEmpty.signalAll(); // Reveiller tout les Threads dormis sur isEmpty
		} finally {
			lock.unlock(); // Deverrouiller
		}
	}

	public Paquet defiler() {
		lock.lock(); // Verrouiller
		try {
			while (this.is_vide()) { // Tant que la fille est vide => dormir sur isEmpty
				try {
					isEmpty.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			Paquet p = this.buffer[this.tete];
			this.tete = (this.tete + 1) % this.size;
			this.taille--;
			isFull.signalAll(); // Reveiller tout les Threads dormis sur isFull
			return p;
		} finally {
			lock.unlock(); // Deverrouiller
		}

	}

	@Override
	public String toString() {
		String str = "Tapis(" + this.taille + "/" + this.size + ") = ";
		if (this.is_vide())
			str += "EMPTY";
		else {
			str += "< ";
			for (int i = this.tete, count = 0; count < this.taille; count++, i = (i + 1) % this.taille) {
				str += this.buffer[i] + " ";
			}
		}
		return str + "\n";
	}

}
