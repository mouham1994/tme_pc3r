public class Consommateur extends Thread {

	private int id;
	public static Integer compteur = 0;
	private Tapis t;

	public Consommateur(int id, Tapis t) {
		this.id = id;
		this.t = t;
	}

	private void consommer() {
		//System.out.println("C" + this.id + " est dans le bloc");
		
		// Consommer un produit de la file
		Paquet p = this.t.defiler();
		System.out.println("C" + this.id + " mange " + p.getData());
		
		// Laisser un seul Consommateur a la fois modifier le compteur
		synchronized (Consommateur.compteur) {
			Consommateur.compteur--;
		}
		
		// Dormir un petit instant aleatoire
		try {
			sleep((int) (Math.random() * 100));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		// Consommer compteur fois de Produits
		while (Consommateur.compteur > 0) {
			this.consommer();
		}
		System.out.println("Le consommateur " + this.id + " a fini son travail");
	}

}
