public class Test {

	public static void main(String[] args) {
		
		if (args.length < 4) {
			
			System.out.println("Erreur : utiliser [ java Test n m file cible ]");
			
		} else {
		
			// Lire les donnees sur la ligne de commande
			int n = Integer.parseInt(args[0]);
			int m = Integer.parseInt(args[1]);
			int capacite = Integer.parseInt(args[2]);
			int cible = Integer.parseInt(args[3]);
			
			// Initialiser le compteur des consommateurs
			Consommateur.compteur = n * cible;
		
			try {
				// Creation d'une nouvelle fille
				Tapis t = new Tapis(capacite);
				
				// Declaration des Threads
				Thread[] p = new Thread[n];
				Thread[] c = new Thread[m];
				
				 // Instantiation des Threads
				for (int i=0; i<n; p[i] = new Producteur("Produit_" + i, cible, t), i++);
				for (int i=0; i<m; c[i] = new Consommateur(i, t), i++);
				
				// Demarrage des Threads
				for (int i=0; i<n; p[i].start(), i++);
				for (int i=0; i<m; c[i].start(), i++);
				
				// Attendre l'arrets des Threads
				for (int i=0; i<n; p[i].join(), i++);
				for (int i=0; i<m; c[i].join(), i++);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}

}
