public class Producteur extends Thread {

	private String nom;
	private int cible;
	private Tapis t;

	public Producteur(String nom, int cible, Tapis t) {
		this.nom = nom;
		this.cible = cible;
		this.t = t;
	}

	private void produire(int i) {
		//System.out.println(this.nom + " est dans le bloc");
		
		// Produire + Mise dans la file un paquet
		this.t.enfiler(new Paquet(this.nom + " " + i));
		System.out.println(this.nom + " " + i);
		
		// Dormir un petit instant aleatoire
		try {
			sleep((int) (Math.random() * 100));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		// Produire cible fois de Produits
		for (int i = 0; i < this.cible; i++) {
			this.produire(i);
		}
		System.out.println("Le producteur de " + this.nom + " a fini son travail");
	}

}
