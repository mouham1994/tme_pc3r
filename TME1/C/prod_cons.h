#ifndef __PROD_CONS__
#define __PROD_CONS__

// Represente les parametres a passer au thread du producteur
typedef struct {
	const char* nom;
	unsigned cible;
} prod;

// Represente les parametres a passer au thread du consommateur
typedef struct {
	int id;
	int* cpt;
} cons;

prod* new_prod(const char* nom, unsigned cible); // Creer un nouveau prod
cons* new_cons(int id, int* cpt); // Creer un nouveau cons
void del_prod(prod* p);
void del_cons(cons* c);

// Producteur/Consommateur
void* producteur(void* p); // Fonction du thread producteur
void* consommateur(void* c); // Fonction du thread consommateur

void produire(const char* nom, int num); // Permet de produire un paquet et l'enfiler dans le tapis en utilisant (mutex et cond)
void consommer(int id, int* cpt);// Permet de retirer un paquet du tapis (en utilisant mutex et cond)

#endif
