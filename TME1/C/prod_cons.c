#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "tapis.h"
#include "prod_cons.h"

// Variables globales
pthread_mutex_t mutex_file = PTHREAD_MUTEX_INITIALIZER; // verrouiller l'acces au tapis
pthread_cond_t can_produce = PTHREAD_COND_INITIALIZER; // Permet a un consommateur de reveiller les producteurs
pthread_cond_t can_consume = PTHREAD_COND_INITIALIZER; // Permet a un producteur de reveiller les consommateurs
tapis* file; // File

prod* new_prod(const char* nom, unsigned cible) {
	prod* p = (prod*) allocate(sizeof(prod));
	p->nom = strdup(nom);
	p->cible = cible;
	return p;
}

cons* new_cons(int id, int* cpt) {
	cons* c = (cons*) allocate(sizeof(cons));
	c->id = id;
	c->cpt = cpt;
	return c;
}

void del_prod(prod* p) {
	free(p);
}

void del_cons(cons* c) {
	free(c);
}

void* producteur(void* p) {
	prod* pr = (prod*) p; // Caster les parametres
	for (int i=0; i<pr->cible; i++) { // Produire "cible" produits portant le nom "nom"
		produire(pr->nom, i);
	}
	printf("%s a fini son travail\n", pr->nom);
	return NULL;
}

void produire(const char* nom, int num) {
	/*
		1) Verrouiller la file
		2) Tantque la file est pleine dormir et attendre le signal can_produce (emis par les consommateurs)
		3) Produire un paquet
		4) ajouter le paquet à la fin de la file
		5) Envoyer un signal (can_consume) aux consommateurs
		6) Déverrouiller la file
	*/
	pthread_mutex_lock(&mutex_file);
	while (is_full(file)) {
		pthread_cond_wait(&can_produce, &mutex_file);
	}
	char str[100];
	sprintf(str, "%s %d\n", nom, num);
	paquet* p = new_paquet(str);
	enfiler(file, p);
	print_paquet(p);
	pthread_cond_signal(&can_consume);
	pthread_mutex_unlock(&mutex_file);
}

void* consommateur(void* c) {

	cons* co = (cons*) c; // Caster les parametres
	while (*(co->cpt) > 0) { // Consommer les produits tant que le compteur est superieur a 0
		consommer(co->id, co->cpt);
	}
	printf("C%d a fini son travail\n", co->id);
	return NULL;
}

void consommer(int id, int* cpt) {
	/*
		1) Verrouiller la file
		2) Tantque la file est vide dormir et attendre le signal can_consume (emis par les producteurs)
		3) Consommer un paquet
		4) Envoyer un signal (can_produce) aux producteurs
		5) Déverrouiller la file
	*/
	pthread_mutex_lock(&mutex_file);
	while (is_vide(file)) {
		pthread_cond_wait(&can_consume, &mutex_file);
	}
	paquet* p = defiler(file);
	*cpt -= 1;
	printf("C%d mange ", id);
	print_paquet(p);
	pthread_cond_signal(&can_produce);
	pthread_mutex_unlock(&mutex_file);
}

int main(int argc, char** argv) {

	int N; // Nombre de producteurs
	int M; // Nombre de consommateur
	int C; // Cible d'un producteur
	int taille; // Taille de la file

	if (argc != 5) { // Valeurs par default
		N = 2;
		M = 1;
		C = 3;
		taille = 2;
	} else { // Valeurs lues en a travers les arguments de main
		N = atoi(argv[1]);
		M = atoi(argv[2]);
		C = atoi(argv[3]);
		taille = atoi(argv[3]);
	}

	int CPT = N * C; // Compteur pour les consommateurs

	pthread_t p[N]; // N pthread producteurs
	pthread_t c[M]; // M pthread consommateurs

	file = new_tapis(taille); // Tapis de taille = taille

	for (int i=0; i<N; i++) { // Lancer N pthread producteurs
		char str[100];
		sprintf(str, "Produit%d", i);
		pthread_create(&p[i], NULL, producteur, new_prod(str, C));
	}

	for (int i=0; i<M; i++) { // Lancer M pthread consommateurs
		pthread_create(&c[i], NULL, consommateur, new_cons(i, &CPT));
	}

	// Attendre la fin de tout les threads pour finir le main
	for (int i=0; i<N; i++) {
		pthread_join(p[i], NULL);
	}
	for (int i=0; i<M; i++) {
		pthread_join(c[i], NULL);
	}

	return 0;
}