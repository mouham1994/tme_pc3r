#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tapis.h"

void* allocate(size_t size) {
	void* mem = malloc(size);
	if (!mem) {
		perror("Error lors de l'allocation avec malloc");
		exit(EXIT_FAILURE);
	}
	return mem;
}

paquet* new_paquet(const char* str) {
	paquet* p = (paquet*) allocate(sizeof(paquet));
	p->str = strdup(str);
	p->next = NULL;
	return p;
}

void del_paquet(paquet* p) {
	free(p);
}

void print_paquet(paquet* p) {
	printf("%s", p->str);
}

tapis* new_tapis(unsigned capacite) {
	tapis* t = (tapis*) allocate(sizeof(tapis));
	t->capacite = capacite;
	t->taille = 0;
	t->tete = NULL;
	t->queue = NULL;
	return t;
}

void del_tapis(tapis* t) {
	paquet* parcours = t->tete;
	while (parcours) {
		paquet* save = parcours;
		parcours = parcours->next;
		del_paquet(save);
	}
	free(t);
}

bool is_vide(tapis* t) {
	return t->taille == 0;
}

bool is_full(tapis* t) {
	return t->taille == t->capacite;
}

void print_tapis(tapis* t) {
	printf("Tapis(%d/%d) = ", t->taille, t->capacite);
	if (is_vide(t)) {
		printf("EMPTY\n");
		return;
	}
	printf("< ");
	paquet* parcours = t->tete;
	while (parcours) {
		print_paquet(parcours);
		printf(" ");
		parcours = parcours->next;
	}
	printf("\n");
}

bool enfiler(tapis* t, paquet* p) {
	if (is_full(t)) return FALSE;
	if (is_vide(t)) t->tete = p;
	else t->queue->next = p;
	t->queue = p;
	t->taille++;
	return TRUE;
}

paquet* defiler(tapis* t) {
	if (is_vide(t)) return NULL;
	paquet* save = t->tete;
	t->tete = t->tete->next;
	t->taille--;
	if (is_vide(t)) t->queue = NULL;
	return save;
}