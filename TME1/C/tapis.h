#ifndef __TAPIS_H__
#define __TAPIS_H__

// Tools
void* allocate(size_t size);
#define TRUE 1
#define FALSE 0
typedef int bool;

// Structures de donnees
typedef struct _paquet { // Representation d'un paquet
	char* str; // Pointeur vers chaine de caractere ( donnee utile du paquet )
	struct _paquet* next; // Paquet suivant dans la liste chainee
} paquet;

typedef struct _tapis { // Representation d'un tapis
	unsigned capacite; // Taille maximale du tapis
	unsigned taille; // Taille actuelle du tapis
	paquet* tete; // Pointeur vers le 1er paquet du tapis
	paquet* queue; // Pointeur vers le 2eme paquet du tapis
} tapis;


// Manipulation du tapis
tapis* new_tapis(unsigned capacite); // Creer un nouveau tapis vide
void del_tapis(tapis* t); // Detruire le tapis
bool is_vide(tapis* t); // Tester si le tapis est vide
bool is_full(tapis* t); // Tester si le tapis est rempli
bool enfiler(tapis* t, paquet* p); // Rajouter un paquet a la fin du tapis, retourne vrai si l'ajout a ete effectue (le tapis n'etais pas plein)
paquet* defiler(tapis* t); // Retirer le premier paquet du tapis et le retourner
void print_tapis(tapis* t); // Afficher le contenu du tapis


//Manipulation du paquet
paquet* new_paquet(const char* str); // Creer un nouveau paquet initialise avec str
void del_paquet(paquet* p); // Detruire le paquet
void print_paquet(paquet* p); // Afficher le paquet

#endif
