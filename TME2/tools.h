#ifndef __TOOLS_H__
#define __TOOLS_H__

void* reallocate(void* buffer, size_t size);
void* allocate(size_t size);

#endif
