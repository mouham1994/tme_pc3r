#ifndef __JOURNAL_H__
#define __JOURNAL_H__

// Journal
typedef struct _journal {
	unsigned size_max;
	unsigned size;
	char** buffer;
} journal;

// Fonction manipulant journal
journal* new_journal(unsigned size_max);
void del_journal(journal* j);
void add_journal(journal* j, const char* data);
void print_journal(journal* j, const char* nom);

#endif
