#ifndef __PROD_CONS__
#define __PROD_CONS__


// Represente les parametres a passer au thread du producteur
typedef struct {
	const char* nom;
	unsigned cible;
	journal* j;
} prod;

// Represente les parametres a passer au thread du consommateur
typedef struct {
	int id;
	int* cpt;
	journal* j;
} cons;

// Represente les parametres a passer au thread du messager
typedef struct {
	int id;
	journal* j;
	int* cpt;
} msg;

// Fonctions manipulant prod
prod* new_prod(const char* nom, unsigned cible, journal* j); // Creer un nouveau prod
void del_prod(prod* p); // del un prod

// Fonctions manipulant cons
cons* new_cons(int id, int* cpt, journal* j); // Creer un nouveau cons
void del_cons(cons* c); // del un cons

// Fonctions manipulant msg
msg* new_msg(int id, int* cpt, journal* j); // Creer un nouveau msg
void del_msg(msg* m); // del un msg

// Producteur / Consommateur / Messager
void producteur(void* p);
void consommateur(void* c);
void messager(void* m);


#endif
