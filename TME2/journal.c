#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"
#include "journal.h"

void del_journal(journal* j) {
	for (int i=0; i<j->size; free(j->buffer[i]), i++);
	free(j->buffer);
	free(j);
}

journal* new_journal(unsigned size_max) {
	journal* j = (journal*) allocate(sizeof(journal));
	j->size_max = size_max;
	j->size = 0;
	j->buffer = (char**) allocate(sizeof(char*) * size_max);
	return j;
}

void add_journal(journal* j, const char* data) {
	if (j->size == j->size_max) {
		j->size_max *= 2;
		j->buffer = (char**) reallocate(j->buffer, sizeof(char*) * j->size_max);
	}
	j->buffer[j->size] = strdup(data);
	j->size++;
}

void print_journal(journal* j, const char* nom) {
	printf("Journal %s (%d/%d) :\n", nom, j->size, j->size_max);
	for (int i=0; i<j->size; i++) {
		printf("%d. %s\n", i, j->buffer[i]);
	}
}
