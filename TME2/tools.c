#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"

void* reallocate(void* buffer, size_t size) {
	void* mem = realloc(buffer, size);
	if (!mem) {
		perror("Error lors de la reallocation avec realloc");
		exit(EXIT_FAILURE);
	}
	return mem;
}

void* allocate(size_t size) {
	void* mem = malloc(size);
	if (!mem) {
		perror("Error lors de l'allocation avec malloc");
		exit(EXIT_FAILURE);
	}
	return mem;
}
