#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include "tools.h"
#include "tapis.h"
#include "journal.h"
#include "prod_cons.h"
#include "/Vrac/MU4IN507/ft_v1.1/include/fthread.h"

tapis* tapis_producteur; // File des producteurs
ft_scheduler_t scheduler_prod; // Scheduler des producteurs
journal* j_prod; // Journal des producteurs

tapis* tapis_cons; // File des consommateurs
ft_scheduler_t scheduler_cons; // Scheduler des consommateurs
journal* j_cons; // Journal des consommateurs

journal* j_msg;

ft_event_t can_produce; // Evenement pour reveiller les producteurs
ft_event_t can_consume; // Evenement pour reveiller les consommateurs

ft_event_t prod_to_msg;
ft_event_t cons_to_msg;


prod* new_prod(const char* nom, unsigned cible, journal* j) {
	prod* p = (prod*) allocate(sizeof(prod));
	p->nom = strdup(nom);
	p->cible = cible;
	p->j = j;
	return p;
}

cons* new_cons(int id, int* cpt, journal* j) {
	cons* c = (cons*) allocate(sizeof(cons));
	c->id = id;
	c->cpt = cpt;
	c->j = j;
	return c;
}

void del_prod(prod* p) {
	free(p);
}

void del_cons(cons* c) {
	free(c);
}

msg* new_msg(int id, int* cpt, journal* j) {
	msg* m = (msg*) allocate(sizeof(msg));
	m->id = id;
	m->cpt = cpt;
	m->j = j;
	return m;
}

void del_msg(msg* m) {
	free(m);
}

void producteur(void* p) {
	prod* pr = (prod*) p;
	char nom[100];
	for (int i=0; i<pr->cible; i++) {
		sprintf(nom, "%s %d", pr->nom, i);
		paquet* pq = new_paquet(nom);
		
		
		while (is_full(tapis_producteur)) { // Essayer d'ajouter le paquet dans la file des producteurs
			printf("%s dors\n", pr->nom);     // Si la file est deja pleine alors dormir et attendre
			fflush(stdout);                   // L'evenement can_produce pour continuer la production 
			ft_thread_await(can_produce);
		}
		
		enfiler(tapis_producteur, pq); // Enfiler le paquet dans la file des producteurs
		
		printf("%s\n", pq->str); // Afficher le paquet enfiler
		fflush(stdout);
		
		add_journal(pr->j, pq->str); // Noter l'action dans le journal
		ft_thread_generate(prod_to_msg); // Reveiller les messages
		ft_thread_cooperate(); // Laisser la main aux autres threads
	}

	printf("%s a fini son travail\n", pr->nom); // Afficher un message de fin de l'execution du thread
	fflush(stdout);
}

void consommateur(void* c) {
	cons* co = (cons*) c;
	
	while ((*co->cpt) > 0) {
		
		while (is_vide(tapis_cons)) {   // Essayer de retirer un paquet dans la file des consommateurs
			printf("C%d dors\n", co->id); // Si la file est deja vide alors dormir et attendre
			fflush(stdout);               // L'evenement can_consume pour continuer la consommation
			ft_thread_await(can_consume);
		}
		
		paquet* pq = defiler(tapis_cons);  // Retirer le paquet du tapis
		printf("C%d mange %s\n", co->id, pq->str);
		fflush(stdout);
		
		char msg[100];
		sprintf(msg, "C%d mange %s", co->id, pq->str);
		add_journal(j_cons, msg); // Noter l'action dans le journal
		(*co->cpt) = (*co->cpt) - 1;
		ft_thread_generate(cons_to_msg); // Reveiller les messages
		ft_thread_cooperate(); // Laisser la main aux autres threads	
	}
	
	printf("C%d a fini son travail\n", co->id); // Afficher un message de fin de l'execution du thread
	fflush(stdout);
}

void messager(void* m) {
	
	msg* ms = (msg*) m;
	
	while ((*ms->cpt) > 0) {
	
		ft_thread_link(scheduler_prod); // Se lier au scheduler des producteurs
		
		while (is_vide(tapis_producteur)) {   // Essayer de retirer un paquet dans la file des producteurs
			printf("C%d dors chez les producteurs\n", ms->id); // Si la file est deja vide alors dormir et attendre
			fflush(stdout);               // L'evenement prod_to_msg pour continuer
			ft_thread_await(prod_to_msg);
		}
		
		paquet* pq = defiler(tapis_producteur); // Defiler un paquet
		printf("M%d a retirer %s\n", ms->id, pq->str); // message de reussite
		fflush(stdout);
		ft_thread_generate(can_produce); // Reveiller les producteurs
		
		ft_thread_cooperate(); // Laisser la main aux autres threads
		ft_thread_unlink(); // Se detacher du scheduler des producteurs
		
		//add_journal(j_msg, ); // #################################### Securiser l'ajout au journal <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		ft_thread_link(scheduler_cons); // Se lier au scheduler des consommateurs
		
		while (is_full(tapis_cons)) {                            // Essayer d'ajouter le paquet dans la file des consommateurs
			printf("C%d dors chez les consommateurs\n", ms->id);   // Si la file est deja pleine alors dormir et attendre
			fflush(stdout);                                        // L'evenement cons_to_msg pour continuer
			ft_thread_await(cons_to_msg);
		}
		
		enfiler(tapis_cons, pq); // Enfiler le paquet chez les consommateurs
		printf("M%d a inserer %s\n", ms->id, pq->str); // message de reussite
		fflush(stdout);
		
		ft_thread_generate(can_consume); // Reveiller les consommateurs
		
		
		ft_thread_cooperate(); // Laisser la main aux autres threads	
		ft_thread_unlink(); // Se detacher du scheduler des consommateurs
		
	
	}
	
	printf("M%d a fini son travail\n", ms->id); // Afficher un message de fin de l'execution du thread
	fflush(stdout);
	
	
}


#define JOURNAL 100
#define CAPACITE 3
#define N 1
#define M 1
#define P 1
#define CIBLE 3

int COMPTEUR = N * CIBLE;


int main(int argc, char** argv) {


	// Gestion initiale des producteurs
	scheduler_prod = ft_scheduler_create(); // Creer un scheduler pour les producteurs
	can_produce = ft_event_create(scheduler_prod);
	prod_to_msg = ft_event_create(scheduler_prod);
	tapis_producteur = new_tapis(CAPACITE); // Creer un tapis pour les producteurs
	j_prod = new_journal(JOURNAL);
	char nom[100];
	for (int i=0; i<N; i++) { // Creer N threads producteurs
		sprintf(nom, "Produit%d", i);
		ft_thread_create(scheduler_prod, producteur, NULL, new_prod(nom, CIBLE, j_prod));
	}
	ft_scheduler_start(scheduler_prod); // Lancer le scheduler des producteurs
	
	
	// Gestion initiale des consommateurs
	scheduler_cons = ft_scheduler_create(); // Creer un scheduler pour les consommateurs
	can_consume = ft_event_create(scheduler_cons);
	cons_to_msg = ft_event_create(scheduler_cons);
	tapis_cons = new_tapis(CAPACITE); // Creer un tapis pour les consommateurs
	j_cons = new_journal(JOURNAL);
	for (int i=0; i<M; i++) { // Creer M threads consommateurs
		ft_thread_create(scheduler_cons, consommateur, NULL, new_cons(i, &COMPTEUR, j_cons));
	}
	ft_scheduler_start(scheduler_cons); // Lancer le scheduler des producteurs
	
	
	// Gestion initiale des messagers
	j_msg = new_journal(JOURNAL);
	for (int i=0; i<P; i++) { // Creer M threads messagers
		ft_thread_create(scheduler_prod, messager, NULL, new_msg(i, &COMPTEUR, j_msg));
	}
	
	ft_scheduler_start(scheduler_prod); // Lancer le scheduler des producteurs
	
	// Detruire toutes les structures allouees
	
	ft_exit();
	return 0;
}
