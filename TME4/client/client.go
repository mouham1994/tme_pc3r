package main

import (
	"fmt"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"time"
	"io/ioutil"
	"strings"
	"net"
	"bufio"
	st "./structures"
	tr "./travaux"
)

//**************************//
//* File de taille limitée *//
//**************************//
type File struct {
	buffer []personne_int
	index int
	size int
	smax int
}

func new_File(smax int) *File {
	return &File{buffer: make([]personne_int, smax), index: 0, size: 0, smax: smax}
}

func (f *File) is_empty() bool {
	return (*f).size == 0
}

func (f *File) is_less_than_half() bool {
	return (*f).size < (*f).smax/2
}

func (f *File) is_full() bool {
	return (*f).size == (*f).smax
}

func (f *File) push(p personne_int) {
	(*f).buffer[(*f).index] = p
	(*f).index = ((*f).index + 1) % (*f).smax
	(*f).size += 1
}

func (f *File) pop() personne_int {
	(*f).index = ((*f).index - 1) % (*f).smax
	(*f).size--
	return (*f).buffer[(*f).index]
}


//***************************//
//* Constantes du programme *//
//***************************//
var ADRESSE string = "localhost"
var FICHIER_SOURCE string = "./conseillers-municipaux.txt"
var TAILLE_SOURCE int = 450000
var TAILLE_G int = 5
var NB_G int = 2
var NB_P int = 2
var NB_O int = 4
var NB_PD int = 2
var pers_vide = st.Personne{Nom: "", Prenom: "", Age: 0, Sexe: "M"}
var LINES []string = []string{}


//***************************//
//* Structures du programme *//
//***************************//
type personne_emp struct {
	pers st.Personne
	ligne  int
	afaire []func(st.Personne) st.Personne
	statut string
}

type msg_proxy struct {
	retour chan string
	methode string
	id int
}

type personne_dist struct {
	id int
	proxy chan msg_proxy
}

type personne_int interface {
	initialise()
	travaille()
	vers_string() string
	donne_statut() string
}

type DataInputLecteur struct {
	ligne int
	canalOutputLecteur chan string
}


//**************************//
//* Déclaration des canaux *//
//**************************//
var chanInLec chan DataInputLecteur
var chanInGes chan personne_int
var chanInGes2 chan personne_int
var chanInOuv chan personne_int
var chanInCol chan personne_int
var fintemps chan int
var result chan string


//********************//
//* Fonctions utiles *//
//********************//
func personne_de_ligne(l string) st.Personne {
	separateur := regexp.MustCompile("\u0009")
	separation := separateur.Split(l, -1)
	naiss, _ := time.Parse("2/1/2006", separation[7])
	a1, _, _ := time.Now().Date()
	a2, _, _ := naiss.Date()
	agec := a1 - a2
	return st.Personne{Nom: separation[4], Prenom: separation[5], Sexe: separation[6], Age: agec}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func read(filename string) string {
	data, err := ioutil.ReadFile(filename)
	check(err)
	return string(data)
}

func buildLINES(filename string) []string {
	file, err := os.Open(filename)
	defer file.Close()
	check(err)
	data := read(file.Name())
	lignes := strings.Split(data, "\n")	
	return lignes[2:]
}

func rand_a_b(min int, max int) int {
	nbre := rand.Intn(max)
	return (nbre % (max-min+1)) + min
}


//**********************************************************************//
//* METHODES DE L'INTERFACE personne_int POUR LES PAQUETS DE PERSONNES *//
//**********************************************************************//
func (p *personne_emp) initialise() { 
	privateCanal := make(chan string)
	msgToLecteur := DataInputLecteur{ligne: (*p).ligne, canalOutputLecteur: privateCanal}
	chanInLec <- msgToLecteur
	line := <- privateCanal
	pers := personne_de_ligne(line)
	(*p).pers = pers
	nbre := rand_a_b(1,5)
	for i:=0; i<nbre; i++ {
		(*p).afaire = append((*p).afaire, tr.UnTravail())
	}
	(*p).statut = "R"
}

func (p *personne_emp) travaille() {
	(*p).pers = (*p).afaire[0]((*p).pers)
	(*p).afaire = (*p).afaire[1:]
	if (len((*p).afaire) == 0) {
		(*p).statut = "C"
	}
}

func (p *personne_emp) vers_string() string {
	return "{ Nom: " + (*p).pers.Nom + ", Prénom: " + (*p).pers.Prenom + ", Age: " + strconv.Itoa((*p).pers.Age) + ", Sexe: " + (*p).pers.Sexe + " }";
}

func (p *personne_emp) donne_statut() string {
	return (*p).statut
}


//*******************************************************************************************//
//* METHODES DE L'INTERFACE personne_int POUR LES PAQUETS DE PERSONNES DISTANTES (PARTIE 2) *//
//*               ces méthodes doivent appeler le proxy (aucun calcul direct)               *//
//*******************************************************************************************//
func (p personne_dist) initialise() {
	local := make(chan string)
	msg := msg_proxy{methode: "initialise", retour: local, id: p.id}
	p.proxy <- msg
	<- local
}

func (p personne_dist) travaille() {
	local := make(chan string)
	msg := msg_proxy{methode: "travaille", retour: local, id: p.id}
	p.proxy <- msg
	<- local
}

func (p personne_dist) vers_string() string {
	local := make(chan string)
	msg := msg_proxy{methode: "vers_string", retour: local, id: p.id}
	p.proxy <- msg
	s := <- local
	return s
}

func (p personne_dist) donne_statut() string {
	local := make(chan string)
	msg := msg_proxy{methode: "donne_statut", retour: local, id: p.id}
	p.proxy <- msg
	s := <- local
	return s
}


//**************************************//
//*** CODE DES GOROUTINES DU SYSTEME ***//
//**************************************//
func proxy(host string, port string) {
	// Partie 2: contacté par les méthodes de personne_dist, le proxy appelle la méthode à travers le réseau et récupère le résultat
	// il doit utiliser une connection TCP sur le port donné en ligne de commande

	add := host + ":" + port
	conn, err := net.Dial("tcp" , add)
	
	if err != nil {
		return
	}
	
	reader := bufio.NewReader(os.Stdin)
	continu := true
	for continu {
		fmt.Println("Entrer une ligne de texte (vide pour stopper) :")
		chaine, _ := reader.ReadString('\n')
		if chaine == "\n" {
			fmt.Fprintf(conn, fmt.Sprint(chaine))
			continu = false
			conn.Close()
		} else {
			fmt.Print("Envoi : " ,chaine)
			fmt.Fprintf(conn, fmt.Sprint(chaine))
			reponse, _ := bufio.NewReader(conn).ReadString('\n')
			reponse = strings.TrimSuffix(reponse, "\n")
			fmt.Println("Recoit : " , reponse)
		}
	}

}

func lecteur() {
	msg := <- chanInLec
	if len(LINES) == 0 {
		LINES = buildLINES(FICHIER_SOURCE) //Lire une fois pour toute tout le fichier
		fmt.Println("J'ai lu le fichier")
	}
	msg.canalOutputLecteur <- LINES[msg.ligne]
}

func producteur() {
	for {
		ligne := rand_a_b(0, TAILLE_SOURCE)
		p := &personne_emp{pers: pers_vide, ligne: ligne, statut: "V"}
		fmt.Println("Producteur à envoyé")
		chanInGes <- p
		time.After(time.Second)
	}
}

func gestionnaire() {
	
	file := new_File(TAILLE_G)
	
	/*
		Si la file est pleine même pas à moitier :
			Si la file est vide : recevoir les paquets des producteurs et des ouvriers
			Sinon recoir les paquets des ouvriers et envoyé les paquets au ouvriers
		Sinon
			Si la fil est pleine : envoyé des paquets au ouvriers
			Sinon recoir les paquets des ouvriers ou envoyé des paquets aux ouvriers
	*/ 
	for {
		
		if file.is_less_than_half() {	
			if file.is_empty() {
				select {
					case p := <- chanInGes:
						fmt.Println("Gestionnaire à reçu : " + p.vers_string())
						file.push(p)
					case p := <- chanInGes2:
						fmt.Println("Gestionnaire à reçu : " + p.vers_string())
						file.push(p)
				}
			} else {	
				select {
					case p := <- chanInGes2:
						fmt.Println("Gestionnaire à reçu : " + p.vers_string())
						file.push(p)
					case chanInOuv <- file.pop():
						fmt.Println("Gestionnaire à envoyé")
				}	
			}
		} else {
			if file.is_full() {
				chanInOuv <- file.pop()
				fmt.Println("Gestionnaire à envoyé")
			} else {
				select {
					case p := <- chanInGes2:
						fmt.Println("Gestionnaire à reçu : " + p.vers_string())
						file.push(p)
					default:
						chanInOuv <- file.pop()
						fmt.Println("Gestionnaire à envoyé")
				}
			}
		}
			
	}

}

func ouvrier() {
	for {
		p := <- chanInOuv
		chanInGes2 <- p
		fmt.Println("Ouvrier à reçu : " + p.vers_string())
		
		statut := p.donne_statut()
		
		if statut == "V" { //Vide
			p.initialise()
			fmt.Println("Ouvrier à envoyé : " + p.vers_string())
			chanInGes2 <- p
		} else if statut == "R" { //En cours de modification
			p.travaille()
			fmt.Println("Ouvrier à envoyé : " + p.vers_string())
			chanInGes2 <- p
		} else { //Fini
			fmt.Println("Ouvrier à fini : " + p.vers_string())
			chanInCol <- p
		}

	}
}

func collecteur() {
	journal := ""
	for {
		select {
			case p := <- chanInCol:
				journal += "\n" + p.vers_string()
				fmt.Println("Collecteur à reçu : " + p.vers_string())
			case <- fintemps:
				result <- journal
		}
	}
}


/*
func producteur_distant(enfiler chan personne_int, port int, proxer msg_proxy, frais chan int) {
	for {
		n := <- frais
		fmt.Println("Producteur Distant crée un producteur avec un identifiant ", n)
		p := personne_dist{id: n, proxy: proxy}
		local := make(chan string)
		proxer <- msg_proxy{id: n, methode: "creer", retour: local}
		<- local
		enfiler <- np
		
	}
}*/


func main() {
	
	rand.Seed(time.Now().UTC().UnixNano()) // graine pour l'aleatoire
	
	if len(os.Args) < 3 {
		fmt.Println("Format: client <port> <millisecondes d'attente>")
		return
	}
	port, _ := strconv.Atoi(os.Args[1])   // utile pour la partie 2
	millis, _ := strconv.Atoi(os.Args[2]) // duree du timeout
	fintemps = make(chan int) // Canal pour interrompre le collecteur 
	result = make(chan string) // Canal du resultat, le collecteur remet son resultat sur ce canal
	
	// A FAIRE
	// creer les canaux : Les canaux sont créer en global
	chanInLec = make(chan DataInputLecteur)
	chanInGes = make(chan personne_int)
	chanInGes2 = make(chan personne_int)
	chanInOuv = make(chan personne_int)
	chanInCol = make(chan personne_int)
	
	// lancer les goroutines (parties 1 et 2): 1 lecteur, 1 collecteur, des producteurs, des gestionnaires, des ouvriers

	go lecteur()
	
	for i:=0; i<NB_P; i++ {
		go producteur()
	}
	
	for i:=0; i<NB_G; i++ {
		go gestionnaire()
	}
	
	for i:=0; i<NB_O; i++ {
		go ouvrier()
	}
	
	go collecteur()
	
	
	// lancer les goroutines (partie 2): des producteurs distants, un proxy
	//fmt.Println("Hello")
	go proxy(ADRESSE, strconv.Itoa(port))
	
	/*
	for i:=0; i<NB_PD; i++ {
		go producteur_distant()
	}
	*/
	
	time.Sleep(time.Duration(millis) * time.Millisecond)
	fintemps <- 0
	fmt.Println("Resultat envoyé par le collecteur :\n" + <- result)
	/*select {
		
	}*/
}
